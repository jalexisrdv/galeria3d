package galeria3d;

import java.util.ArrayList;
import org.newdawn.slick.opengl.Texture;

public class NumerosAleatorios {
    
    /*Se generan numeros aleatorios de acuerdo a un rango y estos nunca
    se repiten, esta clase la utilizo para poner las pinturas de manera
    aleatoria*/

    private int valorInicial;
    private int valorFinal;
    private ArrayList listaNumero;

    public NumerosAleatorios(int valorInicial, int valorFinal) {
        this.valorInicial = valorInicial;
        this.valorFinal = valorFinal;
        listaNumero = new ArrayList();
    }

    private int numeroAleatorio() {
        return (int) (Math.random() * (valorFinal - valorInicial + 1) + valorInicial);
    }

    public int generar() {
        if (listaNumero.size() < (valorFinal - valorInicial) + 1) {
            //Aun no se han generado todos los numeros
            int numero = numeroAleatorio();//genero un numero
            if (listaNumero.isEmpty()) {//si la lista esta vacia
                listaNumero.add(numero);
                return numero;
            } else {//si no esta vacia
                if (listaNumero.contains(numero)) {//Si el numero que generé esta contenido en la lista
                    return generar();//recursivamente lo mando a generar otra vez
                } else {//Si no esta contenido en la lista
                    listaNumero.add(numero);
                    return numero;
                }
            }
        } else {// ya se generaron todos los numeros
            return -1;
        }
    }
    
    public ArrayList<Texture> generarMatrizPinturas() {
        ArrayList<Texture> pinturas = new ArrayList<Texture>();
        int n = valorFinal - valorInicial + 1;
        for (int i = 0; i < n; i++) {
            pinturas.add(Escenario.loadTexture("pinturas/" + "p (" + generar() + ")"));
        }
        return pinturas;
    }

}
