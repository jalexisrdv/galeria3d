package galeria3d;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.util.glu.GLU.*;

public class Camara {

    //Atributos para translacion y rotacion del observador (camara)
    private float x;
    private float y;
    private float z;
    private float rx;
    private float ry;
    private float rz;

    //Congifuracion de la camara
    private float angulo;
    private float aspecto;
    private float cerca;
    private float lejos;

    public Camara(float angulo, float aspecto, float cerca, float lejos) {
        x = 1f;
        z = -1.5f;
        y = rx = rz = 0;
        ry = 90;
        this.angulo = angulo;
        this.aspecto = aspecto;
        this.cerca = cerca;
        this.lejos = lejos;//profundidad
        
        iniciarProyeccion();
    }
    
    private void iniciarProyeccion() {
        //Preparando las matrices de proyeccion y de modelado
        glMatrixMode(GL_PROJECTION);
        //Resetando la matriz de proyeccion
        glLoadIdentity();
        //Activando la perspectiva
        gluPerspective(angulo, aspecto, cerca, lejos);
        //Creando la matriz de modelado
        glMatrixMode(GL_MODELVIEW);
        //Resetando la matriz de modelado
        glLoadIdentity();
    }
    
    //Este metodo nos permite mover la camara
    public void usarVista() {
        //Llamando a las matrices de rotacion y translacion
        //Activamos con uno el eje que deseamos rotar
        glRotatef(rx, 1, 0, 0);
        glRotatef(ry, 0, 1, 0);
        glRotatef(rz, 0, 0, 1);
        glTranslatef(x, y, z);
    }

    /**
     * @return the x
     */
    public float getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(float x) {
        this.x = x;
    }

    /**
     * @return the y
     */
    public float getY() {
        return y;
    }

    /**
     * @param y the y to set
     */
    public void setY(float y) {
        this.y = y;
    }

    /**
     * @return the z
     */
    public float getZ() {
        return z;
    }

    /**
     * @param z the z to set
     */
    public void setZ(float z) {
        this.z = z;
    }

    /**
     * @return the rx
     */
    public float getRx() {
        return rx;
    }

    /**
     * @param rx the rx to set
     */
    public void setRx(float rx) {
        this.rx = rx;
    }

    /**
     * @return the ry
     */
    public float getRy() {
        return ry;
    }

    /**
     * @param ry the ry to set
     */
    public void setRy(float ry) {
        this.ry = ry;
    }

    /**
     * @return the rz
     */
    public float getRz() {
        return rz;
    }

    /**
     * @param rz the rz to set
     */
    public void setRz(float rz) {
        this.rz = rz;
    }
    
    public void movX(float val) {
        z += val * Math.sin(Math.toRadians(ry));
        x += val * Math.cos(Math.toRadians(ry));
    }
    
    public void movY(float val) {
        y += val;
    }
    
    public void movZ(float val) {
        z += val * Math.sin(Math.toRadians(ry + 90));
        x += val * Math.cos(Math.toRadians(ry + 90));
    }
    
    public void rotX(float val) {
        this.rx += val;
    }
    
    public void rotY(float val) {
        this.ry += val;
    }
    
    public void rotZ(float val) {
        this.rz += val;
    }

}
