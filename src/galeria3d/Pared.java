package galeria3d;

import java.util.ArrayList;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;

public class Pared {
    
    //Colores donde poner las texturas
    //rojo, azul, verde, amarillo
    
    public void dibujarCaraFrontal(float tam) {
        glBegin(GL_QUADS);
            //Cara frontal
//            glColor3f(1, 0, 0);//rojo
            glTexCoord2d(0, 0);
            glVertex3f(-tam / 2, -tam / 2, tam / 2);
            glTexCoord2d(0, 1);
            glVertex3f(tam / 2, -tam / 2, tam / 2);
            glTexCoord2d(1, 1);
            glVertex3f(tam / 2, tam / 2, tam / 2);
            glTexCoord2d(1, 0);
            glVertex3f(-tam / 2, tam / 2, tam / 2);
        glEnd();
    }
    
    public void dibujarCaraPosterior(float tam) {
        glBegin(GL_QUADS);
            //Cara posterior
//            glColor3f(0, 1, 0);//verde
            glTexCoord2d(0, 0);
            glVertex3f(-tam / 2, -tam / 2, -tam / 2);
            glTexCoord2d(0, 1);
            glVertex3f(tam / 2, -tam / 2, -tam / 2);
            glTexCoord2d(1, 1);
            glVertex3f(tam / 2, tam / 2, -tam / 2);
            glTexCoord2d(1, 0);
            glVertex3f(-tam / 2, tam / 2, -tam / 2);
        glEnd();
    }
    
    public void dibujarCaraLateralI(float tam) {
        glBegin(GL_QUADS);
            //Cara lateral izquierda
//            glColor3f(0, 0, 1);//azul
            glTexCoord2d(0, 0);
            glVertex3f(tam / 2, -tam / 2, tam / 2);
            glTexCoord2d(0, 1);
            glVertex3f(tam / 2, -tam / 2, -tam / 2);
            glTexCoord2d(1, 1);
            glVertex3f(tam / 2, tam / 2, -tam / 2);
            glTexCoord2d(1, 0);
            glVertex3f(tam / 2, tam / 2, tam / 2);
        glEnd();
    }
    
    public void dibujarCaraLateralD(float tam) {
        glBegin(GL_QUADS);
//            glColor3f(1, 1, 0);//amarillo
            glTexCoord2d(0, 0);
            glVertex3f(-tam / 2, -tam / 2, tam / 2);
            glTexCoord2d(0, 1);
            glVertex3f(-tam / 2, -tam / 2, -tam / 2);
            glTexCoord2d(1, 1);
            glVertex3f(-tam / 2, tam / 2, -tam / 2);
            glTexCoord2d(1, 0);
            glVertex3f(-tam / 2, tam / 2, tam / 2);
        glEnd();
    }
    
    public void dibujarCaraInferior(float tam) {
        glBegin(GL_QUADS);
//            glColor3f(1, 0, 1);//purpura
            glTexCoord2d(0, 0);
            glVertex3f(-tam / 2, tam / 2, tam / 2);
            glTexCoord2d(0, 1);
            glVertex3f(tam / 2, tam / 2, tam / 2);
            glTexCoord2d(1, 1);
            glVertex3f(tam / 2, tam / 2, -tam / 2);
            glTexCoord2d(1, 0);
            glVertex3f(-tam / 2, tam / 2, -tam / 2);
        glEnd();
    }
    
    public void dibujarCaraSuperior(float tam) {
        glBegin(GL_QUADS);
//            glColor3f(0, 0, 1);//cyan
            glTexCoord2d(0, 0);
            glVertex3f(-tam / 2, -tam / 2, tam / 2);
            glTexCoord2d(0, 1);
            glVertex3f(tam / 2, -tam / 2, tam / 2);
            glTexCoord2d(1, 1);
            glVertex3f(tam / 2, -tam / 2, -tam / 2);
            glTexCoord2d(1, 0);
            glVertex3f(-tam / 2, -tam / 2, -tam / 2);
        glEnd();
    }

}
