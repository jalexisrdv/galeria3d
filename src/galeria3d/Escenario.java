package galeria3d;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.lwjgl.opengl.GL11.glRotatef;
import static org.lwjgl.opengl.GL11.glTranslatef;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Escenario {

    ArrayList<Texture> pinturas;
    private Texture texturaPintura;
    private Texture texturaPared;
    private Pared pared;

    public Escenario() {
        pinturas = new NumerosAleatorios(1, 70).generarMatrizPinturas();
        texturaPared = loadTexture("pared (2)");
        pared = new Pared();
    }

    public void dibujarOtrasCarasA() {
        texturaPared.bind();
        pared.dibujarCaraLateralI(1);
        pared.dibujarCaraLateralD(1);
        pared.dibujarCaraSuperior(1);
        pared.dibujarCaraInferior(1);
    }

    public void dibujarParedesA(int x, int z, int p1, int p2) {
        glTranslatef(x, 0, z);
        texturaPintura = pinturas.get(p1);
        texturaPintura.bind();
        pared.dibujarCaraFrontal(1);

        texturaPintura = pinturas.get(p2);
        texturaPintura.bind();
        pared.dibujarCaraPosterior(1);

        dibujarOtrasCarasA();
        glTranslatef(-x, 0, -z);
    }

    public void dibujarOtrasCarasP() {
        texturaPared.bind();
        pared.dibujarCaraLateralI(1);
        pared.dibujarCaraLateralD(1);
        pared.dibujarCaraSuperior(1);
        pared.dibujarCaraInferior(1);
        pared.dibujarCaraFrontal(1);
    }

    public void dibujarParedesP(int x, int z, int p1) {
        glTranslatef(x, 0, z);
        texturaPintura = pinturas.get(p1);
        texturaPintura.bind();
        pared.dibujarCaraPosterior(1);
        dibujarOtrasCarasP();
        glTranslatef(-x, 0, -z);
    }

    public void dibujarOtrasCarasF() {
        texturaPared.bind();
        pared.dibujarCaraLateralI(1);
        pared.dibujarCaraLateralD(1);
        pared.dibujarCaraSuperior(1);
        pared.dibujarCaraInferior(1);
        pared.dibujarCaraPosterior(1);
    }

    public void dibujarParedesF(int x, int z, int p1) {
        glTranslatef(x, 0, z);
        texturaPintura = pinturas.get(p1);
        texturaPintura.bind();
        pared.dibujarCaraFrontal(1);
        dibujarOtrasCarasF();
        glTranslatef(-x, 0, -z);
    }
    
    public void dibujarOtrasCarasS() {
        texturaPared.bind();
        pared.dibujarCaraSuperior(1);
        pared.dibujarCaraInferior(1);
    }

    public void dibujarParedesS(int x, int z, int p1, int p2, int p3, int p4) {
        glTranslatef(x, 0, z);
        texturaPintura = pinturas.get(p1);
        texturaPintura.bind();
        pared.dibujarCaraFrontal(1);

        texturaPintura = pinturas.get(p2);
        texturaPintura.bind();
        pared.dibujarCaraPosterior(1);
        
        texturaPintura = pinturas.get(p3);
        texturaPintura.bind();
        pared.dibujarCaraLateralI(1);
        
        texturaPintura = pinturas.get(p4);
        texturaPintura.bind();
        pared.dibujarCaraLateralD(1);
        dibujarOtrasCarasS();
        glTranslatef(-x, 0, -z);
    }
    
    //Paredes sin pinturas
    public void dibujarParedes(int x, int z) {
        glTranslatef(x, 0, z);
        texturaPared.bind();
        pared.dibujarCaraFrontal(1);
        pared.dibujarCaraPosterior(1);
        pared.dibujarCaraLateralI(1);
        pared.dibujarCaraLateralD(1);
        pared.dibujarCaraSuperior(1);
        pared.dibujarCaraInferior(1);
        glTranslatef(-x, 0, -z);
    }

    public void dibujarEscenario() {
        dibujarParedesA(1, 6, 0, 1);
        dibujarParedesA(2, 4, 2, 3);
        dibujarParedesA(3, 4, 4, 5);
        dibujarParedesA(4, 4, 6, 7);
        dibujarParedesA(6, 2, 8, 9);
        dibujarParedesA(7, 2, 10, 11);
        dibujarParedesA(8, 2, 12, 13);
        dibujarParedesA(8, 4, 14, 15);
        dibujarParedesA(9, 4, 16, 17);
        dibujarParedesA(12, 2, 18, 19);

        dibujarParedesP(4, 8, 20);
        dibujarParedesP(6, 8, 21);
        dibujarParedesP(8, 8, 22);
        dibujarParedesP(10, 8, 23);
        dibujarParedesP(12, 8, 24);
        
        dibujarParedesF(3, 0, 25);
        dibujarParedesF(5, 0, 26);
        dibujarParedesF(7, 0, 27);
        dibujarParedesF(9, 0, 28);
        dibujarParedesF(11, 0, 29);
        
        dibujarParedesS( 2, 2, 30, 31, 32, 33);
        dibujarParedesS( 3, 6, 34, 35, 36, 37);
        dibujarParedesS( 4, 2, 38, 39, 40, 41);
        dibujarParedesS( 5, 6, 42, 43, 44, 45);
        dibujarParedesS( 6, 4, 46, 47, 48, 49);
        dibujarParedesS( 7, 6, 50, 51, 52, 53);
        dibujarParedesS( 9, 6, 54, 55, 56, 57);
        dibujarParedesS(10, 2, 58, 59, 60, 61);
        dibujarParedesS(11, 4, 62, 63, 64, 65);
        dibujarParedesS(11, 6, 66, 67, 68, 69);
        
        dibujarParedes(0, 3);
        dibujarParedes(0, 4);
        dibujarParedes(0, 5);
        dibujarParedes(0, 6);
        dibujarParedes(0, 7);
        dibujarParedes(13, 1);
        dibujarParedes(13, 2);
        dibujarParedes(13, 3);
        dibujarParedes(13, 4);
        dibujarParedes(13, 5);
        dibujarParedes(13, 6);
        dibujarParedes(0, 0);
        dibujarParedes(0, 8);
        dibujarParedes(1, 0);
        dibujarParedes(1, 8);
        dibujarParedes(2, 0);
        dibujarParedes(2, 8);
        dibujarParedes(3, 8);
        dibujarParedes(4, 0);
        dibujarParedes(5, 8);
        dibujarParedes(6, 0);
        dibujarParedes(7, 8);
        dibujarParedes(8, 0);
        dibujarParedes(9, 8);
        dibujarParedes(10, 0);
        dibujarParedes(11, 8);
        dibujarParedes(12, 0);
        dibujarParedes(13, 0);
        dibujarParedes(13, 8);
    }

    public static Texture loadTexture(String key) {
        try {
            return TextureLoader.getTexture("jpg", new FileInputStream(
                    new File("res/" + key + ".jpg")));

        } catch (IOException ex) {
            Logger.getLogger(Main.class
                    .getName()).log(
                            Level.SEVERE, null, ex);
        }
        return null;
    }
    
    

}
