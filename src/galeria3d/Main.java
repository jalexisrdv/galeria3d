package galeria3d;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import static org.lwjgl.opengl.GL11.*;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class Main {

    private static Camara camara;
    private static int posPinturas = 0;
    private static Escenario escenario;

    public static void main(String[] args) {
        initDisplay();
        initGL();
        gameLoop();
        cleanUp();
    }

    public static void initGL() {
        float dimension = (Display.getWidth() / Display.getHeight());
        camara = new Camara(70, dimension, 0.3f, 500);
        escenario = new Escenario();

        glClearColor(0, 0, 0, 1);

        glEnable(GL_DEPTH_TEST);
        //Habilitando luces
//        glEnable(GL_LIGHTING);
        //Habilitando luz cero
//        glEnable(GL_LIGHT0);

//        glEnable(GL_COLOR_MATERIAL);

        glEnable(GL_TEXTURE_2D);
    }

    public static void gameLoop() {
        
        Texture texuraPiso = Escenario.loadTexture("piso (3)");

        while (!Display.isCloseRequested()) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            glShadeModel(GL_SMOOTH);
            glLoadIdentity();
            //Con este metodo le damos indicaciones a la camara (gluLookAt)
            //gluLookAt(-5, 4, 200, 0, -50, 5, 0, 1, 0);
            
            //System.out.println("Display: " + Display.isVisible());

            camara.usarVista();

            texuraPiso.bind();
            glBegin(GL_QUADS);
            {
                glTexCoord2f(0, 0);
                glVertex3f(-0.5f, -0.5f, -0.5f);
                glTexCoord2f(10, 0);
                glVertex3f(-0.5f, -0.5f, 8.5f);
                glTexCoord2f(10, 10);
                glVertex3f(13.5f, -0.5f, 8.5f);
                glTexCoord2f(0, 10);
                glVertex3f(13.5f, -0.5f, -0.5f);
            }
            glEnd();

            escenario.dibujarEscenario();

            controles();
            Display.update();
        }
    }

    /*Aqui se capturan todos los eventos de teclado y raton 
    para podernos mover*/
    public static void controles() {
        boolean adelante = Keyboard.isKeyDown(Keyboard.KEY_UP);
        boolean atras = Keyboard.isKeyDown(Keyboard.KEY_DOWN);
        boolean izquierda = Keyboard.isKeyDown(Keyboard.KEY_LEFT);
        boolean derecha = Keyboard.isKeyDown(Keyboard.KEY_RIGHT);
        boolean subir = Keyboard.isKeyDown(Keyboard.KEY_S);
        boolean bajar = Keyboard.isKeyDown(Keyboard.KEY_B);

        float mx = Mouse.getDX();
        float my = Mouse.getDY();

        mx *= 0.10f;
        my *= 0.10f;

        //Clic izquierdo
        if (Mouse.isButtonDown(0)) {
            camara.rotY(mx);
            camara.rotX(-my);
        }

        if (subir) {
            camara.movY(-0.002f);
        }
        if (bajar) {
            camara.movY(0.002f);
        }
        if (adelante) {
            camara.movZ(0.002f);
        }
        if (atras) {
            camara.movZ(-0.002f);
        }
        if (izquierda) {
            camara.movX(0.002f);
        }
        if (derecha) {
            camara.movX(-0.002f);
        }

        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            cleanUp();
        }
    }

    public static void cleanUp() {
        Display.destroy();
        System.exit(0);
    }

    public static void initDisplay() {
        try {
            Display.setDisplayMode(new DisplayMode(800, 600));
            Display.setTitle("Galeria 3D");
            Display.create();
        } catch (LWJGLException ex) {
            ex.printStackTrace();
            System.exit(0);
        }
    }
    
    public static FloatBuffer asFloatBuffer(float[] arreglo) {
        FloatBuffer fb = BufferUtils.createFloatBuffer(arreglo.length);
        fb.put(arreglo);
        fb.flip();
        return fb;
    }

    public static IntBuffer asIntBuffer(int[] arreglo) {
        IntBuffer ib = BufferUtils.createIntBuffer(arreglo.length);
        ib.put(arreglo);
        ib.flip();
        return ib;
    }
}
