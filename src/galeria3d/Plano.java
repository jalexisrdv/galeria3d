package galeria3d;

public class Plano {

    /**
     * a: se pone una pintura lado izquierdo y derecho 
     * s: se pone una pintura en todas las caras excepto en la inferior y superior 
     * " ": espacios en blanco, representan los pasillos 
     * f: pintura en la parte frontal 
     * p: pintura en la parte posterior
     */
    public static String[][] recorrido() {
        String[][] matriz = {
            {"|", " ", " ", "-", "-", "-", "-", "-", "|"},
            {"|", " ", " ", " ", " ", " ", "a", " ", "|"},
            {"|", " ", "s", " ", "a", " ", " ", " ", "|"},
            {"f", " ", " ", " ", "a", " ", "s", " ", "|"},
            {"|", " ", "s", " ", "a", " ", " ", " ", "p"},
            {"f", " ", " ", " ", " ", " ", "s", " ", "|"},
            {"|", " ", "a", " ", "s", " ", " ", " ", "p"},
            {"f", " ", "a", " ", " ", " ", "s", " ", "|"},
            {"|", " ", "a", " ", "a", " ", " ", " ", "p"},
            {"f", " ", " ", " ", "a", " ", "s", " ", "|"},
            {"|", " ", "s", " ", " ", " ", " ", " ", "p"},
            {"f", " ", " ", " ", "s", " ", "s", " ", "|"},
            {"|", " ", "a", " ", " ", " ", " ", " ", "p"},
            {"|", "-", "-", "-", "-", "-", "-", " ", "|"}
        };
        return matriz;
    }

}
